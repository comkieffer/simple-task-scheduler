
/// @defgroup Task

/**\addtogroup Task
 *  @{
 */

#pragma once

#include <unistd.h>
#include <stdint.h>
#include <math.h>
#include <time.h>

/* Define the task state */
typedef enum task_state
{
    /// Indicates that the task is not intialised yet.
    TASK_STATE_STARTUP,	
    
    /// Indicates that the task is initialised and should repeat.
    TASK_STATE_RUNNING,	

    /// Indicates that the task has terminated successfully and should not repeat.
    TASK_STATE_STOPPED,	

    /// Indicates that the task has failed. it will not be run again until the 
    /// error is cleared.
    TASK_STATE_ERROR	
} task_state;

char const* task_state_str(task_state state);


/**
 * @brief Type for `Task` work functions
 * 
 * This is a function that takes the current `task_state` as an input and 
 * return the desired task state after the function has run. 
 * 
 * If the task returns `TASK_STATE_ERROR` or `TASK_STATE_STOPPED` it will
 * not be executed by the scheduler again until it is set to 
 * `TASK_STATE_STARTUP` or `TASK_STATE_RUNNING` again.
 * 
 * Example:
 * 
 *     task_state task_one_run(task_state state)
 *     {
 *         if (state == TASK_STATE_STARTUP) {
 *             initialise_task( ... );
 *             return TASK_STATE_RUNNING;
 *          }
 *	
 *          if (!do_complex_task( ... )) {
 *              return TASK_STATE_ERROR;
 *          }
 *			
 *          return TASK_STATE_RUNNING;
 *     }
 */
typedef task_state (*task_fn_t)(task_state);

/**
 * @brief Container object for tasks.
 * 
 */
typedef struct 
{
    /// ID of the task, used to identify it in logs
    uint16_t 	id;				

    /// Period at which the task should be repeated (in ns). A value of 0 
    /// denotes a one-shot task. 
    uint32_t 	repeat_period;	 

    /**
     * @brief Time by which the task should be completed. 
     * 
     * If a task is supposed to activate at a time `t0`. Then the task must be
     * finished by time `t0 + deadline` where `deadline` is a count of ns.
     * 
     * If `deadline` is `0`, the task is considered to have no deadline.
     */
    uint32_t 	deadline;		

    /**
     * @brief Current state of the task,
     * 
     * Initially set to `TASK_STATE_STARTUP`. One-shot tasks move straight to 
     * `TASK_STATE_STOPPED` once they finish. Cyclic tasks move to 
     * `TASK_STATE_RUNNING`. Failures can be reported by setting the state to
     * `TASK_STATE_ERROR`. 
     */
    task_state	state;			// State of the task
    
    /// Task work function, executed when the task is running.
    task_fn_t run_fn;

    /// Time at which the task should be activated by the scheduler next
    struct timespec next_activation_time;
} Task;


/* Init the task */
Task* task_create(task_fn_t task_fn, uint16_t task_id, uint32_t period, uint32_t deadline);

/** @}*/

