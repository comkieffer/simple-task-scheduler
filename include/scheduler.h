/**
 * @file scheduler.h
 * @brief Simple scheduler implementation. 
 * 
 * The scheduler simply compares the requested run times of the tasks and 
 * picks the first task that needs to run. There is no support for priorities
 * in the current implementation since they are not needed for the task to 
 * run successfully. 
 */

/// @defgroup Scheduler

/**\addtogroup Scheduler
 *  @{
 */

#pragma once

#include <stdlib.h>
#include <time.h>

#include "task.h"
#include "time_utils.h"

// Uncomment this for more granular feedback on task execution
// #define SCHED_DBG 

/// How long the scheduler should sleep if no tasks are available
#define SCHEDULER_POLL_INTERVAL 10 * MSEC_US

/**
 * @brief Compare two tasks to sort them in a list. 
 * 
 * Called by qsort to order the task list at each tick of the scheduler.
 * 
 * @return int Returns `1` if a should run before `b`, `-1` if `b` should run
 *  before `a` and `0` otherwise.
 */
int task_prio_cmp(const void* a, const void* b);

/**
 * @brief Decide if a task is ready to run or not. 
 * 
 * A task is ready if it's state is not `TASK_STATE_STOPPED` or 
 * `TASK_STATE_ERROR` and the `next_activation_time` is in the past. 
 * 
 * We pass in the clock so that, in a single scheduler pass, all the tasks are
 * compared to the same clock instant. 
 * 
 * @param task The task to evaluate
 * @param clock_now The time that should be used as a reference for whether 
 *  the task is overdue or not.  
 * @return int 
 */
int task_is_ready(Task* task, struct timespec* clock_now);

/**
 * @brief Iterate through the list of tasks and return the next task that 
 *  should run.
 * 
 * @param task_list The list of tasks to process.
 * @param task_count The number of tasks in the list.
 * @return Task* The task that should run or `NULL` if not tasks are ready.
 */
Task* get_next_scheduled_task(Task* task_list[], size_t task_count);

/**
 * @brief Select the next task to run, execute it and update its state.
 * 
 * The next scheduled task is picked with `get_next_scheduled_task`. The 
 * taks `run_fn` is executed and the task state is updated with its return 
 * value. 
 * 
 * If the task blew it's deadline, a warning is produced. 
 * 
 * The function will then update the `next_activation_time` and return.
 * 
 * @param task_list The list of tasks to process.
 * @param task_count The number of tasks in the list.
 */
void tick_scheduler(Task* task_list[], size_t task_count);

/** @}*/

