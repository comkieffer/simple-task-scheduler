#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdint.h>
#include <stdio.h>

void print(char* fmt, ...);

#endif // _UTILS_H_
