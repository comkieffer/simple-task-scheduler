
/// @defgroup Time

/**\addtogroup Time
 *  @{
 */

#include <time.h>
#include <stdint.h>

/// Number of nanoseconds in a millisecond
#define MSEC_NS 1000000 

/// Number of microseconds in a millisecond
#define MSEC_US 1000 

/**
 * @brief Compare two `struct timespec` objects. 
 * 
 * @return int Return `1` if `a > b` (_i.e._ `a` is after `b`),
 * @return int Return `-1` if `a < b` (_i.e_) `a` is before `b`
 * @return int Return `0` if `a == b`. 
 */
int timespec_cmp(struct timespec const* a, struct timespec const* b);

/// Add a milliseconds to a timespec object and return it.
struct timespec timespec_add_millis(struct timespec const* ts, uint32_t millis);

/** @}*/
