#include "catch.hpp"

extern "C" {
    #include "scheduler.h"
}

task_state one_shot_placeholder_fn(task_state state __attribute__((unused))) {
    return TASK_STATE_STOPPED;
}

task_state repeating_placeholder_fn(task_state state __attribute__((unused))) {
    return TASK_STATE_RUNNING;
}

TEST_CASE("Test task_is_ready", "[scheduler]") {
    struct timespec t0 = { .tv_sec = 100, .tv_nsec = 0 };

    SECTION("With STOPPED task") {
        Task* stopped_task = task_create(NULL, 1, 0, 0);
        stopped_task->state = TASK_STATE_STOPPED;

        CHECK(task_is_ready(stopped_task, &t0) == 0);
    }

    SECTION("With RUNNING task in the past") {
        Task* running_task = task_create(NULL, 1, 0, 0);
        running_task->state = TASK_STATE_RUNNING;
        running_task->next_activation_time = { .tv_sec = 0, .tv_nsec = 0 };


        CHECK(task_is_ready(running_task, &t0) == 1);
    }
    
    SECTION("With RUNNING task in the future") {
        Task* running_task = task_create(NULL, 1, 0, 0);
        running_task->state = TASK_STATE_RUNNING;
        running_task->next_activation_time = { .tv_sec = 200, .tv_nsec = 0};

        CHECK_FALSE(task_is_ready(running_task, &t0));
    }

}


TEST_CASE("Test tick_scheduler", "[scheduler]") {
    SECTION("With only a one-shot task") {
        Task* one_shot_task = task_create(one_shot_placeholder_fn, 1, 0, 0);
        Task* task_list[] = {one_shot_task};

        tick_scheduler(task_list, 1);
        CHECK(one_shot_task->state == TASK_STATE_STOPPED);
    }

    SECTION("With only one repeating task") {
        Task* repeating_task = task_create(repeating_placeholder_fn, 1, 500 * MSEC_NS, 0);
        Task* task_list[] = {repeating_task};

        tick_scheduler(task_list, 1);
        CHECK(repeating_task->state == TASK_STATE_RUNNING);

        // Ensure that the next activation time has been updated. 
        CHECK(repeating_task->next_activation_time.tv_nsec > 0);
    }

    SECTION("With a mix of tasks") {
        Task* one_shot_task = task_create(one_shot_placeholder_fn, 1, 0, 0);
        Task* repeating_task = task_create(repeating_placeholder_fn, 2, 500 * MSEC_NS, 0);
        Task* task_list[] = {one_shot_task, repeating_task};
        int task_count = sizeof(task_list) / sizeof(task_list[0]);

        // Control the start times of the two tasks to ensure that we control
        // their sort order in the run queue.
        one_shot_task->next_activation_time = { .tv_sec = 0, .tv_nsec = 0};
        repeating_task->next_activation_time = { .tv_sec = 1, .tv_nsec = 0};
        
        // At the first run, we expect the one shot task to run first since it 
        // the first item in the list and all the activation time are in the 
        // past. 
        Task* next_task = get_next_scheduled_task(task_list, task_count);
        CHECK(next_task == one_shot_task); 

        // Run the scheduler once to advance
        tick_scheduler(task_list, task_count);
        CHECK(one_shot_task->state == TASK_STATE_STOPPED);

        // The next time we run the scheduler, we expec the repeating task to 
        // be selected.
        CHECK(get_next_scheduled_task(task_list, task_count) == repeating_task); 
    }
}
