#include "catch.hpp"

extern "C" {
    #include "time_utils.h"
}

TEST_CASE("Test timespec_cmp", "[scheduler]") {
    struct timespec t0 = { .tv_sec = 0, .tv_nsec = 0 };
    struct timespec t1 = { .tv_sec = 0, .tv_nsec = 100 };
    struct timespec t2 = { .tv_sec = 10, .tv_nsec = 0 };

    CHECK(timespec_cmp(&t0, &t0) == 0);
    
    // Test when only tv_nsec is different
    CHECK(timespec_cmp(&t0, &t1) == -1);
    CHECK(timespec_cmp(&t1, &t0) == 1);

    // Test when tv_sec is different
    CHECK(timespec_cmp(&t0, &t2) == -1);
    CHECK(timespec_cmp(&t2, &t0) == 1);
}

int timespec_cmp_wrapper(void const* a, void const* b) {
    return timespec_cmp((struct timespec const*) a, (struct timespec const*) b);
}

TEST_CASE("Test timespec_cmp as qsort comparator", "[scheduler]") {
    struct timespec t0 = { .tv_sec = 0, .tv_nsec = 0 };
    struct timespec t1 = { .tv_sec = 0, .tv_nsec = 100 };
    struct timespec t2 = { .tv_sec = 10, .tv_nsec = 0 };

    struct timespec* times[] = {&t1, &t0, &t2};
    qsort(times, 3, sizeof(times[0]), timespec_cmp_wrapper);

    CHECK(times[0] == &t0);
    CHECK(times[1] == &t1);
    CHECK(times[2] == &t2);
}

TEST_CASE("Test timespec_add", "[scheduler]") {

    SECTION("Without nsec overflow") {
        struct timespec t0 = { .tv_sec = 0, .tv_nsec = 0 };
        struct timespec t1 = { .tv_sec = 0, .tv_nsec = 100 * MSEC_NS };

        struct timespec t0_plus_100ms = timespec_add_millis(&t0, 100);
        CHECK(t0_plus_100ms.tv_sec == t1.tv_sec);    
        CHECK(t0_plus_100ms.tv_nsec == t1.tv_nsec);    
    }
    
    SECTION("With nsec overflow") {
        struct timespec t0 = { .tv_sec = 0, .tv_nsec = 100 * MSEC_NS };
        struct timespec t1 = { .tv_sec = 1, .tv_nsec = 100 * MSEC_NS };

        struct timespec t0_plus_1 = timespec_add_millis(&t0, 1000);
        CHECK(t0_plus_1.tv_sec == t1.tv_sec);    
        CHECK(t0_plus_1.tv_nsec == t1.tv_nsec);
    }


    SECTION("With large nsec overflow") {
        struct timespec t0 = { .tv_sec = 0, .tv_nsec = 100 * MSEC_NS };
        struct timespec t1 = { .tv_sec = 1, .tv_nsec = 300 * MSEC_NS };

        struct timespec t0_plus_1 = timespec_add_millis(&t0, 1200);
        CHECK(t0_plus_1.tv_sec == t1.tv_sec);    
        CHECK(t0_plus_1.tv_nsec == t1.tv_nsec);
    }
}