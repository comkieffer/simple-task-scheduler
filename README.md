# Task Scheduler

[Docs](https://comkieffer.gitlab.io/simple-task-scheduler)

## Getting Started

The program uses the `meson` build system. To build, run the following commands:

``` console
$ meson setup builddir
$ cd builddir && ninja
```

To run the test suite, run

``` console
$ meson test
```

To run the main executable 

``` console 
$ ./task-scheduler
```

## Potential Improvements

More complete test suite. The current test suite was mostly a development tool. For continued development it would need to be expanded to have much greater code coverage. 

### Task Management

- Allocate tasks to a static stack at build time instead of allocating them all on the heap at run time. 

### Scheduler

- schedule tasks based on their deadline (to pick the task with the closest deadline.
- implement a basic priority mechanism, only using the deadline to schedule tasks within the same priority group. 
