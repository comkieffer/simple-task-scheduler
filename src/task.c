
#include <assert.h>
#include <stdlib.h>


#include "task.h"

char const* task_state_str(task_state state) {
	// Note: Do not add default catch all clause. 
	// If you do, we won't get `-Wswitch` warnings telling us that the 
	// `switch` is non-exhaustive. 
	switch(state) {
		case TASK_STATE_STARTUP:
			return "STARTUP (0)";
		case TASK_STATE_RUNNING:
			return "RUNNING (1)";
		case TASK_STATE_STOPPED:
			return "STOPPED (2)";
		case TASK_STATE_ERROR:
			return "ERROR (4)";
	}

	// If we reach here, we're misusing the function and passing in something 
	// that isn't a valid `task_state`.
	assert(0);
}

// TODO: Allocate tasks on a fixed size arena.
Task* task_create(task_fn_t task_fn, uint16_t task_id, uint32_t period, uint32_t deadline) {
	Task* new_task = malloc(sizeof(Task));
	*new_task = (const Task){
		.id = task_id, 
		.repeat_period = period,
		.deadline = deadline,
		.state = TASK_STATE_STARTUP,
		.run_fn = task_fn,
		.next_activation_time = { 0 }
	};

	clock_gettime(CLOCK_MONOTONIC_RAW, &new_task->next_activation_time);

	return new_task;
}


