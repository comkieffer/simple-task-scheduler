#include <stdarg.h>

#include "print.h"
#include "time_utils.h"

/**
 * @brief Wrapper around `printf` that prepends the time to the message.
 * 
 * @param fmt Format string for printf
 * @param ... varargs for printf
 */
void print(char* fmt, ...)
{
    struct timespec clock_now;
    clock_gettime(CLOCK_MONOTONIC_RAW, &clock_now);

	va_list args;
    va_start(args, fmt);
    printf("%.4ld:%.9ld: ", clock_now.tv_sec, clock_now.tv_nsec);
	vprintf(fmt, args);
    va_end(args);
}
