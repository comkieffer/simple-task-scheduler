#include <math.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

#include "task.h"
#include "scheduler.h"

/// How long do you want the program to run for ? 
#define RUN_TIME 5  


/* Do some work */
void do_simple_work(int iterations, double scale_factor, double offset)
{	
	for(uint8_t i = 0; i < iterations; i++)
	{
		double random = offset + (((double) rand()) / RAND_MAX * scale_factor);
		
		// mark the result as volatile to prevent the compiler from 
		// optimising it away.
		volatile double res = log(random);
	}
}

task_state task_one_run(task_state state)
{	
	if (state == TASK_STATE_STARTUP) {
		usleep(100 * MSEC_US);
	}
	
	do_simple_work(100, 1.0, 0.0);
	return TASK_STATE_STOPPED;
}

task_state task_two_run(task_state state)
{	
	if (state == TASK_STATE_STARTUP) {
		usleep(150 * MSEC_US);
	}

	do_simple_work(100, 10.0, 1.0);
	return TASK_STATE_RUNNING;
}

task_state task_three_run(task_state state __attribute__((unused)))
{	
	do_simple_work(100, 100.0, 1.0);
	return TASK_STATE_RUNNING;
}


void system_init() 
{
	srand(time(NULL));
}

int main()
{
	system_init();

	Task* task_one =   task_create(task_one_run, 1, 0, 0); 
	Task* task_two =   task_create(task_two_run, 2, 100 * MSEC_NS, 500 * MSEC_NS);
	Task* task_three = task_create(task_three_run, 3, 500 * MSEC_NS, 700 * MSEC_NS);

	Task* task_list[] = {task_one, task_two, task_three};
	size_t task_count = sizeof(task_list) / sizeof(task_list[0]);

	/* Main scheduler */
	struct timespec start_time;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start_time);

	while (1) {
		tick_scheduler(task_list, task_count);

		struct timespec time_now;
		clock_gettime(CLOCK_MONOTONIC_RAW, &time_now);
		if (time_now.tv_sec - start_time.tv_sec > RUN_TIME) {
			break;
		}
	} 

	return EXIT_SUCCESS;
}
