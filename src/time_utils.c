
#include "time_utils.h"

int timespec_cmp(struct timespec const* a, struct timespec const* b) {
    if (a->tv_sec > b->tv_sec) {
        return 1;
    } else if (a->tv_sec < b->tv_sec) {
        return -1;
    } 

    // If tv_sec is equal, try to differentiate them by tv_nsec
    if (a->tv_nsec > b->tv_nsec) {
        return 1;
    } else if (a->tv_nsec < b->tv_nsec) {
        return -1;
    } 

    return 0;
}


struct timespec timespec_add_millis(struct timespec const* ts, uint32_t millis) {
    struct timespec new_ts = *ts;

    // Figure out the total number of nanoseconds. 
    long total_nsec = ts->tv_nsec + (millis * MSEC_NS);

    // And then the number of seconds that fit in the number of nanoseconds.
    // This number will then be substracted from the ns count. 
    time_t additional_secs = total_nsec / 1e9;

    new_ts.tv_sec += additional_secs;
    new_ts.tv_nsec = total_nsec - (additional_secs * 1e9);

    return new_ts;
}
