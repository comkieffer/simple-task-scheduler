
#include <assert.h>
#include <stdlib.h>

#include "print.h"
#include "scheduler.h"

int task_prio_cmp(const void* a, const void* b) {
    Task const* ta = *(Task const**) a;
    Task const* tb = *(Task const**) b;

    int cmp = timespec_cmp(
        &ta->next_activation_time, 
        &tb->next_activation_time
    );

    // printf("(%d - %p) %ld.%.9ld > (%d - %p) %ld.%.9ld ? %d\n",
    //     ta->id,
    //     ta,
    //     ta->next_activation_time.tv_sec, 
    //     ta->next_activation_time.tv_nsec,
    //     tb->id,
    //     tb, 
    //     tb->next_activation_time.tv_sec, 
    //     tb->next_activation_time.tv_nsec, 
    //     cmp
    // );

    return cmp;
}
    
int task_is_ready(Task* task, struct timespec* clock_now) {    
    switch (task->state) {
        case TASK_STATE_STOPPED:
        case TASK_STATE_ERROR:
            return 0;

        case TASK_STATE_STARTUP:
        case TASK_STATE_RUNNING:
            // Only run if the next activation time is in the past
            return timespec_cmp(clock_now, &task->next_activation_time) == 1;
    }

    // We should never get here. If we add new states, then we should get a compiler
    // warning that the switch is not exhaustive. 
    assert (0);
    return 0;
}

Task* get_next_scheduled_task(Task* task_list[], size_t task_count) {
    qsort((void*) task_list, task_count, sizeof(task_list[0]), task_prio_cmp);
    
    #ifdef SCHED_DBG
    printf("Sorted Task List (len =  %lu): [", task_count);
    for (size_t idx =0; idx < task_count; ++idx) {
        printf(
            "id: %d (t_next: %.4ld:%.9ld), ", 
            task_list[idx]->id, 
            task_list[idx]->next_activation_time.tv_sec, 
            task_list[idx]->next_activation_time.tv_nsec
        );
    }
    printf("]\n");
    #endif

    struct timespec clock_now;
    clock_gettime(CLOCK_MONOTONIC_RAW, &clock_now);
    
    for (size_t idx = 0; idx < task_count; ++idx) {
        if (task_is_ready(task_list[idx], &clock_now)) {
            return task_list[idx];
        } 
    }

    return NULL;
}

void tick_scheduler(Task* task_list[], size_t task_count) {
    Task* next_task = get_next_scheduled_task(task_list, task_count);
    if (next_task) {
        print("[SCHDL] Executing task <%d>\n", next_task->id);

        if (next_task->run_fn) {
            next_task->state = next_task->run_fn(next_task->state);
        }

        // Check that we finished before the task deadline
        if (next_task->deadline != 0) {
            struct timespec clock_now; 
            clock_gettime(CLOCK_MONOTONIC_RAW, &clock_now);

            struct timespec deadline_time = timespec_add_millis(
                &next_task->next_activation_time, next_task->deadline
            );
            if (timespec_cmp(&clock_now, &deadline_time) == 1) {
                print(
                    "[SCHDL] WARNING: Task <%d> missed deadline. (Deadline = %d.%.9d, Actual = %d.%.9d)\n", 
                    next_task->id, deadline_time.tv_sec, deadline_time.tv_nsec, 
                    clock_now.tv_sec, clock_now.tv_nsec
                );
            }
        }
        
        // Set the time that the task should be woken at next
        if (next_task->repeat_period > 0) {
            next_task->next_activation_time = timespec_add_millis(
                &next_task->next_activation_time, next_task->repeat_period
            );
        }

        #ifdef SCHED_DBG
        print("[SCHDL] Task <%d> completed.\n", next_task->id);
        printf("                     tsk->state = %s\n", task_state_str(next_task->state));
        printf(
            "                     tsk->next_activation_time = %.4ld.%.9ld\n", 
            next_task->next_activation_time.tv_sec, 
            next_task->next_activation_time.tv_nsec
        );
        #endif
    } else {
        #ifdef SCHED_DBG
        print("[SCHDL] No ready tasks. Sleeping.\n");
        #endif

        usleep(SCHEDULER_POLL_INTERVAL);
    }
}